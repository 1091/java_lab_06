import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.lang.Math;
 
public class Lab_6{
    public static void main(String[] args){
        Prog_GUI gui = new Prog_GUI();
        gui.setVisible(true);
    }
}

class Prog_GUI extends JFrame {
	private JButton draw_button = new JButton("Draw");

	private JLabel label_Frequency_A = new JLabel("Frequency A:");
	private JLabel label_Frequency_B = new JLabel("Frequency B:");
	private JLabel label_Shift_X     = new JLabel("Shift X:");
	private JLabel label_Shift_Y     = new JLabel("Shift Y:");
	private JLabel label_Number      = new JLabel("Number:");
	private JLabel label_Radius      = new JLabel("Radius:");
	
	// Spinner Frequency A & B								SpinnerNumberModel(Initial value,  Min value, Max value, Step)
	private JSpinner spinner_Frequency_A = new JSpinner(new SpinnerNumberModel(2.0, 0.0, 100.0, 1.0));
	private JSpinner spinner_Frequency_B = new JSpinner(new SpinnerNumberModel(3.0, 0.0, 100.0, 1.0));        
    // Spinner Shifts X & Y
	private JSpinner spinner_Shift_X     = new JSpinner(new SpinnerNumberModel(0.0, 0.0, 100.0, 1.0));
	private JSpinner spinner_Shift_Y     = new JSpinner(new SpinnerNumberModel(3.14, 0.0, 100.0, 1.0)); 
    // Spinner Number
	private JSpinner spinner_Number      = new JSpinner(new SpinnerNumberModel(1000, 0, 9999999, 1));
    // Spinner Radius
	private JSpinner spinner_Radius      = new JSpinner(new SpinnerNumberModel(5, 0, 100, 1));
	
	private DrawFrame frame = new DrawFrame();
	
	public Prog_GUI(){	
		
		super("lab_6");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(600, 150);
        
        Container container = this.getContentPane();
        container.setLayout(new GridLayout( 4,	// rows
		        						    4,	// columns
		        						   10,	// hgap - �������������� ������
		        						    2	// vgap - ������������ ������
        								)
        );

        container.add(label_Frequency_A);
        container.add(spinner_Frequency_A); 
        container.add(label_Frequency_B);
        container.add(spinner_Frequency_B);
        
        container.add(label_Shift_X);
        container.add(spinner_Shift_X);
        container.add(label_Shift_Y);
        container.add(spinner_Shift_Y);
        
        container.add(label_Number);
        container.add(spinner_Number);
        
        container.add(label_Radius);
        container.add(spinner_Radius);
        
        draw_button.addActionListener(new ButtonEventListener());        
        container.add(draw_button);
	}

	class ButtonEventListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			frame.setValues(Double.parseDouble(spinner_Frequency_A.getValue().toString()),
							Double.parseDouble(spinner_Frequency_B.getValue().toString()),
							Double.parseDouble(spinner_Shift_X.getValue().toString()),
							Double.parseDouble(spinner_Shift_Y.getValue().toString()),
							Integer.parseInt(spinner_Number.getValue().toString()),
							Integer.parseInt(spinner_Radius.getValue().toString()));
			if(frame.isVisible())
				frame.repaint();
			else
				frame.setVisible(true);

		}
	}

}
class DrawFrame extends JFrame{
	private int DEFAULT_WIDTH = 1024;
	private int DEFAULT_HEIGHT = 1024;
	// ���������
    private double amplitude_A = 0;
    private double amplitude_B = 0;
    // �������
    private double frequency_A = 2;
    private double frequency_B = 3;
    // �����
    private double shift_X = 0;
    private double shift_Y = Math.PI;        
    // ���������� ���������
    private int number_Points = 4000;
    // ������ �����
    private int circle_Radius = 5;
	
    public DrawFrame(){
        setTitle("������ �������");
        this.setBounds(600, 0, DEFAULT_WIDTH, DEFAULT_HEIGHT);
        add(new DrawPanel());
    }
    
    public void setValues(double freq_a, double freq_b, double sh_x, double sh_y, int N, int R){
    	frequency_A = freq_a;
    	frequency_B = freq_b;
    	shift_X = sh_x;
    	shift_Y = sh_y;
    	number_Points = N;
    	circle_Radius = R;
    }
    class DrawPanel extends JPanel{
        public void paintComponent(Graphics g){
            super.paintComponent(g);
            Graphics2D g2 = (Graphics2D)g;
            amplitude_A = this.getSize().getWidth() / 2;
            amplitude_B = this.getSize().getHeight() / 2;
            for(int t = 0; t <= number_Points; t++){
            	int x = (int)(amplitude_A - amplitude_A * Math.sin(frequency_A * t + shift_X)) - circle_Radius/2;
            	int y = (int)(amplitude_B - amplitude_B * Math.sin(frequency_B * t + shift_Y)) - circle_Radius/2;
            	g.setColor(Color.RED);
            	g.fillOval(x,y,circle_Radius,circle_Radius);
            }
    	}
    }
}
 

